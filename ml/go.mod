module demo/ml

go 1.16

require (
	github.com/asafschers/goscore v0.0.0-20190823112107-6e6e174c153a
	github.com/confluentinc/confluent-kafka-go v1.6.1
	github.com/mattn/go-shellwords v1.0.11 // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.6.1 // indirect
)

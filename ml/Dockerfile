# Original inspiration: https://github.com/confluentinc/confluent-kafka-go/issues/461
FROM golang:alpine AS builder

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=1  \
    GOARCH="amd64" \
    GOOS=linux   

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Install Compile plug

RUN apk -U add ca-certificates
RUN apk update && apk upgrade && apk add pkgconf git bash build-base sudo
RUN git clone --depth 1 --branch v1.6.1 https://github.com/edenhill/librdkafka.git && cd librdkafka && ./configure --prefix /usr && make && make install

# Copy the code into the container
COPY flowers.go .
COPY internal/neural_network.pmml /neural_network.pmml

# Build the application. jmahonin: Needs '-tags musl' to compile.
RUN go build -tags musl --ldflags "-extldflags -static" -o main .

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

#WORKDIR /src

# Copy binary from build to main folder
RUN cp /build/main .


# Build a small image
FROM scratch

COPY --from=builder /dist/main /
COPY --from=builder /neural_network.pmml /neural_network.pmml
 
# Run our scoring routine, pass custom path and server
ENTRYPOINT ["/main", "-model.path", "/neural_network.pmml", "-bootstrap.server", "broker:29092"]

import org.apache.spark
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature._
import org.apache.spark.ml.classification.RandomForestClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
//import org.jpmml.sparkml.PMMLBuilder

/*
To be run from a Spark shell with classpath setup for now
 */

// Load the CSV as a dataframe
val irisDF = spark
  .read
  .format("csv")
  .option("header", "true")
  .option("inferSchema", "true")
  .load("/Users/jmahonin/devel/personal/demo/ml/training/Iris.csv.gz")

// Split the dataset into 80% train, 20% test
val Array(train, test) = irisDF.randomSplit(Array(0.8, 0.2)

// Create the vector assembler for our feature columns
val assembler = new VectorAssembler()
  .setInputCols(Array("sepal_length", "sepal_width", "petal_length", "petal_width"))
  .setOutputCol("raw")

// Create the indexer, which will load into 'features' column
val indexer = new VectorIndexer()
  .setInputCol("raw")
  .setOutputCol("features")
  .setMaxCategories(4)

// Index our labels from 'class' to 'label'
val labelIndexer = new StringIndexer()
  .setInputCol("class")
  .setOutputCol("label")

// Create the classifier model, using our 'features' and 'label' columns
val classifier = new RandomForestClassifier()
  .setFeaturesCol("features")
  .setLabelCol("label")

// Create the pipeline out of our previous stages
val pipeline = new Pipeline()
  .setStages(Array(assembler, indexer, labelIndexer, classifier))

// Fit the model
val model = pipeline.fit(train)

// Make some predictions
val pred = model.transform(test)

// Evaluate the model
val evaluator = new MulticlassClassificationEvaluator()
  .setLabelCol("label")
  .setPredictionCol("prediction")
  .setMetricName("accuracy")

evaluator.evaluate(pred)

// Export to PMML
//val pmml = new PMMLBuilder(irisSchema, pipelineModel).build()





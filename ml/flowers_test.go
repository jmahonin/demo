package main

import "testing"

var modelPath = "./internal/neural_network.pmml"

func TestScoring(t *testing.T) {
	model := loadModel(&modelPath)
	// Generate features
	input := map[string]interface{}{}
	input["Sepal.Length"] = 5.1
	input["Sepal.Width"] = 3.5
	input["Petal.Length"] = 1.4
	input["Petal.Width"] = 0.2

	// Score for each output possibility
	score0, score1, score2 := scoreIt(model, input)
	t.Log("scores", score0, score1, score2)
}
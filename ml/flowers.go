package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/asafschers/goscore"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"io/ioutil"
)

func main() {
	// Parse command line flags
	kafkaURL, _, modelPath := parseArgs()

	// Load model XML
	model := loadModel(modelPath)

	consumerTopic := "flowers"
	producerTopic := "flowers-scored-test2"

	// Setup Kafka consumer
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": *kafkaURL,
		"group.id":          "flowers-consumer",
		"auto.offset.reset": "latest",
	})

	if err != nil {
		panic(err)
	}

	// Setup Kafka producer
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": *kafkaURL})
	if err != nil {
		panic(err)
	}

	defer p.Close()

	c.SubscribeTopics([]string{consumerTopic}, nil)

	for {
		msg, err := c.ReadMessage(-1)
		if err == nil {
			fmt.Printf("Message on %s: %s\n", msg.TopicPartition, string(msg.Value))
			var parsed map[string]interface{}
			json.Unmarshal(msg.Value, &parsed)

			// Rename fields into a new feature map
			input := map[string]interface{}{}
			input["Sepal.Length"] = parsed["sepal_length"]
			input["Sepal.Width"] = parsed["sepal_width"]
			input["Petal.Length"] = parsed["petal_length"]
			input["Petal.Width"] = parsed["petal_width"]

			// Score it!
			score0, score1, score2 := scoreIt(model, input)

			// Augment original payload with scores
			parsed["score0"] = score0
			parsed["score1"] = score1
			parsed["score2"] = score2

			// Add classification here too
			if score0 >= score1 && score0 >= score2 {
				parsed["likely_score"] = score0
				parsed["likely_type"] = "Iris-setosa"
			} else if score1 >= score0 && score1 >= score2 {
				parsed["likely_score"] = score1
				parsed["likely_type"] = "Iris-versicolor"
			} else {
				parsed["likely_score"] = score2
				parsed["likely_type"] = "Iris-virginica"
			}

			// Turn our map back into JSON
			jsonStr, err := json.Marshal(parsed)
			if err != nil {
				panic(err)
			}

			// Write out augmented payload
			p.Produce(&kafka.Message{
				TopicPartition: kafka.TopicPartition{Topic: &producerTopic, Partition: kafka.PartitionAny},
				Value:          []byte(jsonStr),
			}, nil)

		} else {
			// The client will automatically try to recover from all errors.
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
		}
	}

	c.Close()
}

func parseArgs() (kafkaURL *string, kafkaOutputTopic *string, modelPath *string) {
	kafkaURL = flag.String("bootstrap.server", "localhost:9092", "Kafka Bootstrap Server URL")
	kafkaOutputTopic = flag.String("output.topic", "flowers-scored", "Output Kafka Topic")
	modelPath = flag.String("model.path", "./internal/neural_network.pmml", "Path to PMML model")

	flag.Parse()

	fmt.Println("kafkaURL", *kafkaURL)
	fmt.Println("kafkaOutputTopic", *kafkaOutputTopic)
	fmt.Println("modelPath", *modelPath)

	return
}

func scoreIt(model *goscore.NeuralNetwork, features map[string]interface{}) (score0 float64, score1 float64, score2 float64) {
	score0, _ = model.Score(features, "0")
	score1, _ = model.Score(features, "1")
	score2, _ = model.Score(features, "2")
	return
}

func loadModel(modelPath *string) *goscore.NeuralNetwork {
	// Load model XML
	modelXml, err := ioutil.ReadFile(*modelPath)
	if err != nil {
		panic(err)
	}
	// Build model (NN)
	model, err := goscore.NewNeuralNetwork(modelXml)
	if err != nil {
		panic(err)
	}

	return model
}

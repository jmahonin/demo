#!/bin/bash

# Pre-check, make sure we've got a nice environment...

# Set up Kafka topics (no longer needed)

# Set up producer for flowers
curl -X POST -H "Content-Type: application/json" --data @kafka-connect/flowers.config http://localhost:8083/connectors

# Set up producer for prices
curl -X POST -H "Content-Type: application/json" --data @kafka-connect/prices.config http://localhost:8083/connectors

# Set up KSQL queries

# Set up Kibana index pattern
# curl -X POST api/saved_objects/_import?createNewCopies=true -H "kbn-xsrf: true" --form file

# Producer is started from Logstash

# ???

# Profit!
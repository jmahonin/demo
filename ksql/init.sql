# Create the KSQL stream from scored flowers topic
CREATE STREAM scored_flowers(
	  claimed_type VARCHAR, 
	  likely_score DOUBLE, 
	  likely_type VARCHAR, 
	  petal_length DOUBLE, 
	  petal_width DOUBLE, 
	  score0 DOUBLE, 
	  score1 DOUBLE, 
	  score2 DOUBLE, 
	  sepal_length DOUBLE, 
	  sepal_width DOUBLE, 
	  timestamp INT
	) WITH (
	  kafka_topic='flowers-scored-test2', 
          VALUE_FORMAT='json'
	);

# Create the KSQL table for prices
CREATE TABLE prices (
     timestamp INT,
     type VARCHAR PRIMARY KEY,
     price DOUBLE
   ) WITH (
     KAFKA_TOPIC = 'prices', 
     VALUE_FORMAT = 'JSON'
   );

# Join the streams up up. Need to daisy-chain in multiple streams, KSQL does not support multi-joins
# Create the first stream with the claimed price included

CREATE STREAM flowers_join_p1 AS
SELECT f.TIMESTAMP, f.CLAIMED_TYPE, f.LIKELY_TYPE, f.LIKELY_SCORE, p1.PRICE
FROM  scored_flowers f 
JOIN  PRICES p1 ON f.CLAIMED_TYPE = p1.TYPE;

# Create the final stream from the previous, plus the inferred type / price
CREATE STREAM flowers_with_prices AS
SELECT p1.F_TIMESTAMP AS TIMESTAMP, p1.CLAIMED_TYPE, p1.LIKELY_TYPE, p1.LIKELY_SCORE, p1.PRICE AS CLAIMED_PRICE, p2.PRICE AS LIKELY_PRICE
FROM  flowers_join_p1 p1
JOIN  PRICES p2 ON p1.LIKELY_TYPE = p2.TYPE;

# Find arbitrage opportunities between the listed price of the supposed flower, and the inferred price
CREATE STREAM flowers_arbitrage AS
SELECT TIMESTAMP, (LIKELY_PRICE - CLAIMED_PRICE) AS ARB_DIFF, CLAIMED_TYPE, CLAIMED_PRICE, LIKELY_TYPE, LIKELY_PRICE, LIKELY_SCORE
FROM flowers_with_prices 
WHERE LIKELY_PRICE > CLAIMED_PRICE AND LIKELY_SCORE > 0.95;


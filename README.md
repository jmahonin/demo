# Tulip Mania!

## Summary

The market has gone crazy for tulips! As a gardening enthusiast and software developer, not only do you know a good tulip when you see it, but you've trained a machine to recognize them at scale.

With the market for tulips being so active, sometimes the types listed for sale are incorrectly labelled. If you can spot the mixups first, there's an opportunity for arbitrage and profit!

## Project Overview

This self-contained repository simulates multiple aspects required for capitalizing on the market for tulips:

- Container runtime infrastructure
- Data generation
- Reading/writing from a message queue
- Using an ML classifier model
- Stream processing / enrichment
- Persisting to a search database + dashboarding

### Data Flow

![Data Flow](diagram.png)


### Container Runtime

All running components are deployed using [docker-compose](https://docs.docker.com/compose/)

The YML file itself is composed from Confluent's Platform Quick Start [docker containers](https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html) as well as an open-source ELK (Elasticsearch, Logstash, Kibana) [stack](https://elk-docker.readthedocs.io).

### Data Generation

Sample data is generated using Confluent's [kafka-connect-datagen](https://github.com/confluentinc/kafka-connect-datagen) module, provided as part of the platform runtime. The data schemas and metadata are custom, and are located in the `./kafka-connect` folder as Avro schema (`.avsc`) and datagen `.config` files.

### Message Queue

This project uses Apache Kafka, as provided by the Confluent Platform, as a message queue. 

The purpose of a message queue is to decouple the data producer from downstream consumers, and also enable on-stream message processing.

### ML Classifier

This example uses the iconic [Iris](http://dmg.org/pmml/pmml_examples/index.html#Iris) classification model, with a scoring runtime written in Go, leveraging the [goscore](github.com/asafschers/goscore) library.

In short, flower attributes (petal / sepal dimensions) are fed into the model which then classifies the type of flower it likely is. We'll use the output of the classification to try identify potentially misclassified (and therefore lucrative) flowers.

### Stream Processing

With multiple sources of real-time data (i.e.: inventory and market prices), the purpose of our stream processing module is to combine these feeds together in order to determine market arbitrage opportunities.

The market inventory is defined by a 'flowers' topic, as populated by the data generation module. Our ML classifier uses this as an input, then enriches the payload with flower classifications to a 'flowers-scored' topic.

Using KSQL as a streaming engine, we've defined a KSQL Stream based on the 'flowers-scored' topic. This allows for easily and dynamically querying the stream for messages that match a predicate.

We've also defined a KSQL Table, based on the 'prices' topic, which is an auto-generated randomized item and price. In this case, the flower _type_ is defined as the topic key, which maps to the KSQL Table primary key. Thus, the KSQL Table will always contain the most recent price for each flower.

We can also leverage the KSQL notion of a JOIN between Streams and Tables, which allows us to enrich the 'flowers-scored' payload further, including the current price of the claimed flower type, as well as the price of the likely actual type. In doing so, we can trivially calculate the difference between prices to find arbitrage opportunities.

The topic 'flowers_arbitrage' contains the output of the stream which determines likely arbitrage opportunities. Note that this is also defined as a KSQL Stream, which allows us to quickly adjust the threshold of likelihood for arbitrage if desired.

### Search and Reporting

What good is a data platform without a good dashboard? For this application, we're using an ELK stack (Elasticsearch, Logstash, Kibana)

The arbitrage event data is pulled from the 'flowers_arbitrage' topic using Logstash. It does some very light massaging using a Logstash input/filter/output configuration, and then writes the output to an Elasticsearch index.

A Kibana index pattern is built around this index, and a sample dashboard is included with some interesting metrics / aggregations displayed.


![Example](dashboard.png)
